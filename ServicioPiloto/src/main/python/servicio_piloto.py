#!/usr/bin/env python3
from sys import path

path.append("/../python")

from server_piloto import Servidor

if __name__ == "__main__":
    servidor = Servidor()
    servidor.iniciar()

