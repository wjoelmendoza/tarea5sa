# ESB
Este servidor contiene la información del resto de servicios y solo sirve como intermediario entre los clientes que solicitan el servicio y los servidores que contienen la lógica. Esta disponible en http://host:8080

## Recursos

### /ubicación/&laquo;id_carro&raquo;
Este recurso se puede acceder usando el verbo **GET** y se encarga de redirigir la peticion al servicio de [Rastreo](ServicioRastreo/docs/README.md)


### /piloto/&laquo;id_piloto&raquo;
Este recurso se puede acceder usando el verbo **GET** y se encarga de redirigir la petición al servicio [Piloto](ServicioPiloto/docs/README.md)


### /solicitud/
Este recurso se puede acceder usando el verbo **POST** y encarga de redigir la pericion al Servicio Cliente por lo que debe de cumplir con los parametros descritos en dicho [servicio](ServicioCliente/docs/README.md)

