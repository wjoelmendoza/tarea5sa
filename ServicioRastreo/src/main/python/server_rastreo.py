from flask import Flask
from flask_restful import Resource, Api
from random import randint


class Ubicacion(Resource):
    """
    Esta clase hereda de Resource lo cual permite manejar
    los recursos del api REST segun el verbo que se le indique
    """

    def __init__(self):
        """
        En este constructor se define la coleccion
        de carros validos para el servicio
        """
        Resource()
        '''
        Colección de carros validos
        '''
        self.CARROS = {
            '0': 'Mitsubishi Evolution',
            '1': 'Mazada 3',
            '2': 'Toyota Corolla',
            '3': 'Toyota Echo',
            '4': 'Hummer',
            '5': 'Hyunday Terracan',
            '6': 'Pegeut 203',
            '7': 'Picanto',
            '8': 'Hiunday SantaFe',
            '9': 'Mitsubishi Eclipse'
        }

    def get(self, id_carro):
        """
        Se encarga de revisar si existe el carro en el servicio
        :param id_carro:
        :return: un diccionario con la informacion del automovil:
        {
            'id': id_carro,
            'desc': descripcion,
            'latitud': num,
            'latitud': num
        }
        """

        carro = self.CARROS.get(id_carro, None)

        latitud = randint(-100, 100)
        longitud = randint(-100, 100)

        ubicacion = {
            'id': id_carro,
            'desc': carro,
            'latitud': latitud,
            'longitud': longitud
         }

        return ubicacion


class Servidor:
    """
    Esta clase se encarga de inicializar las diferentes
    herramientas para crear el api rest
    """
    def __init__(self):
        """
        crear el servidor web junto con los recursos
        REST
        """
        self.app = Flask(__name__)
        self.api = Api(self.app)
        self.api.add_resource(Ubicacion, '/ubicacion/<id_carro>')

    def iniciar(self):
        """
        Se encarga de iniciar el servidor web
        :return: None
        """
        '''
        host = 0.0.0.0 indica que escuche en todas las interfaces de red
        port = 8083 indica en que puerto tiene que escuchar
        debug = True permite mensajes de error mas especificos los cuales se muestan en la consola
        '''
        self.app.run(host='0.0.0', port=8083, debug=True)

