# Tarea2 y 3 SA

Esta tarea tiene como objetivo simular un ESB para la comunicación de diferentes servicios

las tecnologías utlizadas son:

* __python 3.7.4__: Es un lenguaje de programación libre, su sitio [web](https://www.python.org/)
* __flask 1.0.2__: Es un microframework que permite crear sevidores web ligeros sobre el lenguaje python, este es su sitio [web](https://palletsprojects.com/p/flask/)
* __flask-restful  0.3.7__: Esta tecnología añade soporte para crear apis basadas en REST sobre el mircoframework [flask](https://flask-restful.readthedocs.io/en/latest/)

El estilo del código esta basado en __pep8__

La estructura de la tarea se basa en 4 servicios REST 3 de los cuales contienen la lógica de negocio y uno que simula a un ESB.

Se realizo un programa de consola que simula ser el cliente.

![Diagrama](img/Tarea2.png)

*diagrama de componentes*

## Descripción general de los servicios
* [ESB](/ESB/docs/README.md): Es una simulación de un ESB y se encarga de redireccionar las peticiones a su correspondiente servicio
* [Servicio Cliente](ServicioCliente/docs/README.md): Este servicio se encargada de recibir las solicitudes y solicitar al ESB un piloto
* [Servicio Piloto](ServicioPiloto/docs/README.md): Este servicio se encarga de retornar el nombre de un piloto junto con información del automovil a la vez este se encarga de solicitar la posición al ESB para retornarla a quién solicito la información
* [Servicio Rastreo](ServicioRastero/docs/README.md): Este servicio retorna una posición junto con la descripción, del automovil
* [Cliente](cliente/docs/README.md): este un programa sencillo que consume el servicio de Cliente.
