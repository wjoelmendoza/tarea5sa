#!/usr/bin/env python3
from requests import post
from random import randint
from colorama import init, Fore, Back, Style
import json
init()


class Usuario:

    def ingresar_dato(self):
        """
        Solicita un id de usuario para realizar la peticion al servicio
        :return: None
        """

        try:
            entrada = input("Ingresa un id: ")

            while True:
                self.hacer_solicitud(entrada)
                entrada = input("Ingresa un id: ")

        except EOFError:
            print("")
            return

    def hacer_solicitud(self, id_cliente):
        """
        Se encarga de realizar la solicitud al ServicioCliente segun el id ingresado
        """
        conexion = "http://localhost:8080/solicitud"
        longitud = randint(-100, 100)
        latitud = randint(-100, 100)

        dt_cliente = {
            'id': id_cliente,
            'latitud': latitud,
            'longitud': longitud
        }

        dt = post(conexion, data=dt_cliente).json()

        print("datos del servicio")
        strval = json.dumps(dt, indent=4)
        print(Fore.GREEN + strval)
        print(Style.RESET_ALL)


if __name__ == "__main__":
    usr = Usuario()
    usr.ingresar_dato()

