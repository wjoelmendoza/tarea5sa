from pybuilder.core import use_plugin, init

use_plugin("python.core")
use_plugin("python.install_dependencies")
use_plugin("python.flake8")
use_plugin("python.distutils")


name = "Cliente"
version = "1.0.1"
default_task = ["publish", "analyze"]


@init
def set_properties(project):
    project.depends_on('colorama')

    project.set_property('flake8_verbose_output', True)
    project.set_property('flake8_include_scripts', True)
